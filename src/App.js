import React, {Component} from 'react';
import './App.css';
import {Icon} from 'react-fa'

function Stylesheets(url) {
    return (<link rel="stylesheet" href={url}/> );
}
let removeObject = function(arr, attr, value){
    let i = arr.length;
    while(i--){
        if( arr[i]
            && arr[i].hasOwnProperty(attr)
            && (arguments.length > 2 && arr[i][attr] === value ) ){
            arr.splice(i,1);
        }
    }
    return arr;
};

let changeObject = function(array, value, question,answer ) {
    for (let i in array) {
        if (array[i].id == value) {
            array[i].question = question;
            array[i].answer = answer;
            break;
        }
    }
    return array;
};

class AddQuestion extends Component {

    constructor(props) {
        super(props);
        this.state = {
            status: this.props.visiblElements.add,
            question: '',
            answer: ''
        };
        this.handleChange = this.handleChange.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            status:nextProps.visiblElements.add
        });
       // console.log(this.state);
    }

    handleChange(event) {
        const target = event.target;
        const name = target.name;
        const value = target.value;
        this.setState({
            [name]: value
        })
    }

    handleAddClick() {
        this.props.addQuestion(this.state.question, this.state.answer);
        this.setState({
            question: '',
            answer: ''
        });
    }

    handleButton() {
        const enabledButton = <button className="btn btn-lg  btn-primary " onClick={() => this.handleAddClick()}
                                      name="submit" type="submit">
            <i className="fa fa-plus"/>
        </button>;
        const disabledButton = <button className="btn btn-lg disabled btn-primary" name="submit" type="submit">
            <i className="fa fa-plus"/>
        </button>;
        if (this.state.question && this.state.answer) {
            return enabledButton;
        } else {
            return disabledButton
        }
    }

    render() {
        const element = <div className="container-fluid">
            <div className="row col-sm-12">
                <div className="panel panel-success">
                    <div className="panel-body">
                        <div className="col-md-12 col-sm-12 col-xs-12">
                            <div className="col-md-5 col-sm-5 col-lg-5">
                                <div className="form-group ">
                                    <label className="control-label " htmlFor="edit-question">
                                        Question
                                    </label>
                                    <input className="form-control"
                                           name="question"
                                           value={this.state.question}
                                           onChange={this.handleChange} type="text"/>
                                </div>
                            </div>
                            <div className="col-md-6 col-sm-6 col-lg-6 padding-right">
                                <div className="form-group ">
                                    <label className="control-label " htmlFor="edit-answer">
                                        Answer
                                    </label>
                                    <input className="form-control"
                                           name="answer"
                                           value={this.state.answer}
                                           onChange={this.handleChange} type="text"/>
                                </div>
                            </div>
                            <div className="col-md-1 col-sm-1 col-lg-1 text-left margin-top">
                                <div className="form-group">
                                    <div>
                                        {this.handleButton()}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>;
        if (this.state.status) {
            return element;
        } else {
            return null;
        }
    }
}

class EditQuestion extends Component {
    constructor(props) {
        super(props);
        this.state = {
            status: this.props.visiblElements.edit,
            id:this.props.currentQuestion.id,
            question:this.props.currentQuestion.question,
            answer:this.props.currentQuestion.answer
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleClick = this.handleClick.bind(this);
    }
    componentWillReceiveProps(nextProps) {
        this.setState({
            status:nextProps.visiblElements.edit,
            id:nextProps.currentQuestion.id,
            question:nextProps.currentQuestion.question,
            answer:nextProps.currentQuestion.answer
        });
    }
    handleChange(event) {
        const target = event.target;
        const name = target.name;
        const value = target.value;
        this.setState({
            [name]: value
        });
    }
    handleClick(event){
        let questionObject = {
            id: this.state.id,
            question:this.state.question,
            answer:this.state.answer
        };
        this.props.editQuestion(questionObject);
    }
    resetDOM(){
        this.props.resetDOM();
    }
    render() {
        const element = (
            <div className="container-fluid">
                <div className="row col-sm-12">
                    <div className="panel panel-warning">
                        <div className="panel-body">
                            <div className="col-md-12 col-sm-12 col-xs-12">
                                <div className="col-md-5 col-sm-5 col-lg-5">
                                    <div className="form-group ">
                                        <label className="control-label " htmlFor="edit-question">
                                            Question
                                        </label>
                                        <input className="form-control" value={this.state.question} onChange={this.handleChange} id="edit-question" name="question" type="text"/>
                                    </div>
                                </div>
                                <div className="col-md-5 col-sm-5 col-lg-5 padding-right">
                                    <div className="form-group ">
                                        <label className="control-label " htmlFor="edit-answer">
                                            Answer
                                        </label>
                                        <input className="form-control" value={this.state.answer} onChange={this.handleChange} name="answer" id="edit-answer" type="text"/>
                                    </div>
                                </div>
                                <div className="col-md-1 col-sm-1 col-lg-1 text-right margin-top">
                                    <div className="form-group">
                                        <div>
                                            <button className="btn btn-lg btn-info " onClick={this.handleClick} name="submit" type="submit">
                                                <i className="fa fa-pencil"/>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-1 col-sm-1 col-lg-1 text-left margin-top">
                                    <div className="form-group">
                                        <div>
                                            <button className="btn btn-lg btn-default " onClick={() => this.resetDOM()} name="submit" type="submit">
                                                <i className="fa fa-times"/>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
        if (this.state.status) {
            return element;
        } else {
            return null;
        }

    }
}

class DeleteQuestion extends Component {
    constructor(props) {
        super(props);
        this.state = {
            status: this.props.visiblElements.delete,
            QuestionPair: this.props.currentQuestion
        };
    }
    componentWillReceiveProps(nextProps) {
        this.setState({
            status:nextProps.visiblElements.delete,
            QuestionPair:nextProps.currentQuestion
        });
        //console.log(this.state);
    }
    deleteConfirm(){
        this.props.deleteQuestion(this.state.QuestionPair);
    }
    resetDOM(){
        this.props.resetDOM();
    }
    render() {

        const element = (
            <div className="container-fluid">
                <div className="row col-sm-12">
                    <div className="panel panel-danger">
                        <div className="panel-body">
                            <div className="row">
                                <div className="col-md-12 col-sm-12 col-lg-12">
                                    <div className="text-center">
                                        <strong> Do you want to delete this element ? </strong>
                                    </div>
                                </div>
                                <div className="col-md-12 col-sm-12 col-lg-12 margin-top-question">
                                    <div className="col-sm-6 col-md-6 col-lg-6 text-right padding-right">
                                        <button className="btn btn-danger btn-width" onClick={() => this.deleteConfirm()}><i className="fa fa-trash-o"/>
                                        </button>
                                    </div>
                                    <div className="col-sm-6 col-md-6 col-lg-6 text-left padding-left">
                                        <button className="btn btn-default btn-width" onClick={() => this.resetDOM()}><i className="fa fa-times"/>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
        if (this.state.status) {
            return element;
        } else {
            return null;
        }
    }
}

class QuestionsTableData extends Component {

    handleEdit() {
        let tableData = JSON.parse(this.refs.editbutton.getAttribute('data-edit'));
        this.props.editQuestion(tableData);
    }
    handleDelete() {
        let tableData = JSON.parse(this.refs.deletebutton.getAttribute('data-delete'));
        this.props.deleteQuestion(tableData);
    }
    render() {
        let tableData = this.props.tableData;
        //console.log(tableData);
        return (
            <tr>
                <td>{tableData.question}</td>
                <td>{tableData.answer}</td>
                <td>
                    <button className="btn btn-info" data-edit={JSON.stringify(tableData)} ref="editbutton"
                            onClick={() => this.handleEdit()}><i className="fa fa-pencil"/></button>
                    <button className="btn btn-danger" data-delete={JSON.stringify(tableData)} ref="deletebutton"
                            onClick={() => this.handleDelete()}><i className="fa fa-trash-o"/></button>
                </td>
            </tr>
        );
    }
}

class QuestionsTable extends Component {

    constructor(props) {
        super(props);
        this.state = {
            status: this.props.visiblElements.table,
            tableData: this.props.tableData
        };
        this.deleteQuestion = this.deleteQuestion.bind(this);
        this.editQuestion = this.editQuestion.bind(this);
    }
    componentWillReceiveProps(nextProps) {
        this.setState({
            status:nextProps.visiblElements.table
        });
        //console.log(this.state);
    }
    deleteQuestion(object) {
        this.props.deleteQuestion(object);
    }
    editQuestion(object) {
        this.props.editQuestion(object);
    }
    renderTableData(tableData) {
        let rows = [];
        for (let i = 0; i < tableData.length; i++) {
            rows.push(<QuestionsTableData editQuestion={this.editQuestion.bind(this)}
                                          deleteQuestion={this.deleteQuestion.bind(this)} key={i}
                                          tableData={tableData[i]}/>);
        }
        return (
            rows
        );
    }
    render() {
        const element = (
            <div className="container-fluid">
                <div className="row col-sm-12">
                    <div className="panel panel-info">
                        <div className="panel-heading">
                            Questions &amp; Answers
                        </div>
                        <div className="panel-body">
                            <table className="table">
                                <thead>
                                <tr>
                                    <th>Question</th>
                                    <th>Answer</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                {this.renderTableData(this.state.tableData)}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        );
        if (this.state.status) {
            return element;
        } else {
            return null;
        }
    }

}

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            tableData: [{
                id: 1,
                question: 'This is a Question ?',
                answer: 'This is an answer'
            }, {
                id: 2,
                question: 'This is a Question 2 ?',
                answer: 'This is an answer 2'
            }
            ],
            visibleElements: {
                add: true,
                edit: false,
                delete: false,
                table: true
            },
            setObject: {
                id:'',
                question:'',
                answer:''
            }
        };
        this.addQuestionAnswerPair = this.addQuestionAnswerPair.bind(this);
        this.editQuestion = this.editQuestion.bind(this);
        this.editQuestionConfirm = this.editQuestionConfirm.bind(this);
        this.deleteQuestion = this.deleteQuestion.bind(this);
        this.deleteQuestionConfirm = this.deleteQuestionConfirm.bind(this)
        this.resetDOM = this.resetDOM.bind(this);
    }


    addQuestionAnswerPair(question, answer) {
        let rows = this.state.tableData;
        rows.unshift({
            id: Math.random(),
            question: question,
            answer: answer
        });
        this.setState({
            tableData: rows
        })
    }

    editQuestion(object) {
        let visibility = this.state.visibleElements;
        visibility.edit = true;
        visibility.table = true;
        visibility.add = false;
        visibility.delete = false;
        console.log(object);
        this.setState({
            tableData: this.state.tableData,
            visibleElements: visibility,
            setObject:object
        });
        console.log('data reached home');
    }

    editQuestionConfirm(object){
        let tableData =this.state.tableData;
        tableData = changeObject(tableData,object.id,object.question,object.answer);
        let visibility = this.state.visibleElements;
        visibility.edit = false;
        visibility.table = true;
        visibility.add = true;
        visibility.delete = false;
        this.setState({
            tableData: tableData,
            visibleElements: visibility,
            setObject:this.state.setObject
        });
    }

    deleteQuestion(object) {
        let visibility = this.state.visibleElements;
        visibility.edit = false;
        visibility.table = true;
        visibility.add = false;
        visibility.delete = true;
        this.setState({
            tableData: this.state.tableData,
            visibleElements: visibility,
            setObject:object
        });
    }

    deleteQuestionConfirm(object){
        let tableData = removeObject(this.state.tableData,'id',object.id);
        let visibility = this.state.visibleElements;
        visibility.edit = false;
        visibility.table = true;
        visibility.add = true;
        visibility.delete = false;
        this.setState({
            tableData: tableData,
            visibleElements: visibility,
            setObject:this.state.setObject
        });
    }

    resetDOM(){
        let visibility = {
            add: true,
            edit: false,
            delete: false,
            table: true
        };
        this.setState({
            visibleElements: visibility
        });
    }

    render() {

        return (
            <div className="App">
                {Stylesheets('https://formden.com/static/cdn/bootstrap-iso.css')}
                <div className="bootstrap-iso container margin">
                    <div className="panel panel-primary">
                        <div className="panel-heading">Tasqat Flow</div>
                        <div className="panel-body">
                            <div className="container-fluid">
                                <AddQuestion
                                    visiblElements={this.state.visibleElements}
                                    addQuestion={this.addQuestionAnswerPair.bind(this)}/>
                                <EditQuestion
                                    visiblElements={this.state.visibleElements}
                                    currentQuestion={this.state.setObject}
                                    editQuestion={this.editQuestionConfirm.bind(this)}
                                    resetDOM={this.resetDOM.bind(this)}/>
                                <DeleteQuestion
                                    visiblElements={this.state.visibleElements}
                                    currentQuestion={this.state.setObject}
                                    deleteQuestion={this.deleteQuestionConfirm.bind(this)}
                                    resetDOM={this.resetDOM.bind(this)}/>
                                <QuestionsTable
                                    visiblElements={this.state.visibleElements}
                                    tableData={this.state.tableData}
                                    editQuestion={this.editQuestion.bind(this)}
                                    deleteQuestion={this.deleteQuestion.bind(this)}/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default App;
